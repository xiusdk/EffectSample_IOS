/*----------------------------------------------------------------------------------------------
 *
 * This file is XIU's property. It contains XIU's trade secret, proprietary and
 * confidential information.
 *
 * The information and code contained in this file is only for authorized XIU employees
 * to design, create, modify, or review.
 *
 * DO NOT DISTRIBUTE, DO NOT DUPLICATE OR TRANSMIT IN ANY FORM WITHOUT PROPER AUTHORIZATION.
 *
 * If you are not an intended recipient of this file, you must not copy, distribute, modify,
 * or take any action in reliance on it.
 *
 * If you have received this file in error, please immediately notify XIU and
 * permanently delete the original and any copy of any file and any printout thereof.
 * (c) www.xiusdk.cn
 *---------------------------------------------------------------------------------------------*/

#ifndef YN_FACE_TRACK_H_
#define YN_FACE_TRACK_H_
 
#include "YNTypes.h"


/// create face detection instance
YN_SDK_API YNHandle YNFaceTrack_Initialize(int config);


/// destroy face detection instance
YN_SDK_API void  YNFaceTrack_Uninitialize(YNHandle handle);


/// load face detection model
YN_SDK_API int YNFaceTrack_LoadModels(YNHandle handle, const char* trackModel);


/// detect face with image buffer
/// param:
/// handle: handle created by YNFaceDetector_Initialize
/// image: image buffer
/// with,height,stride: image info
/// faces: return face detect result
/// faceCount: return face count
YN_SDK_API int YNFaceTrack_Align(YNHandle handle, const unsigned char* image, int width, int height, int stride,
	YN_PIXEL_FORMAT format, YN_ROTATION_TYPE orientation, YNFaces** faces, int* faceCount);


/// track face with image buffer
YN_SDK_API int YNFaceTrack_Track(YNHandle handle, const unsigned char* image, int width, int height, int stride,
	YN_PIXEL_FORMAT format, YN_ROTATION_TYPE orientation, YNFaces** faces, int* faceCount);


#endif//YN_FACE_DETECTOR_H_
