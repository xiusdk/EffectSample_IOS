//
//  YNEffectType.h
//  YNEffectSDK
//
//  Created by xiu on 2017/6/20.
//  Copyright (C) 2017 ZHF. All rights reserved.

#ifndef YNEffectType_h
#define YNEffectType_h

typedef enum YNBeautyMode
{
    YNBeautyMode_Default = 0,
    YNBeautyMode_Mode1 = 1,
    YNBeautyMode_Mode2 = 2,
    YNBeautyMode_Mode3 = 3
}YNBeautyMode;

#endif /* YNEffectType_h */
