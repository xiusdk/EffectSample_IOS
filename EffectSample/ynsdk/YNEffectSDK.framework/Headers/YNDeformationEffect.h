/*----------------------------------------------------------------------------------------------
 *
 * This file is XIU's property. It contains XIU's trade secret, proprietary and
 * confidential information.
 *
 * The information and code contained in this file is only for authorized XIU employees
 * to design, create, modify, or review.
 *
 * DO NOT DISTRIBUTE, DO NOT DUPLICATE OR TRANSMIT IN ANY FORM WITHOUT PROPER AUTHORIZATION.
 *
 * If you are not an intended recipient of this file, you must not copy, distribute, modify,
 * or take any action in reliance on it.
 *
 * If you have received this file in error, please immediately notify XIU and
 * permanently delete the original and any copy of any file and any printout thereof.
 * (c) www.xiusdk.cn
 *---------------------------------------------------------------------------------------------*/

//
//  YNDeformationEffect.h
//  YNEffectSDK
//
//  Created by xiu on 2018/6/20.
//  Copyright (C) 2018 ZHF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YNEffect.h"
#import <UIKit/UIKit.h>

@interface YNDeformationEffect : YNEffect

@property (nonatomic) NSNumber* eyeIntensity;
@property (nonatomic) NSNumber* noseIntensity;
@property (nonatomic) NSNumber* chinIntensity;
@property (nonatomic) NSNumber* foreheadIntensity;
@property (nonatomic) NSNumber* smallFaceIntensity;
@property (nonatomic) NSNumber* cutFaceIntensity;
@property (nonatomic) NSNumber* movNoseIntensity;
@property (nonatomic) NSNumber* zoomMouthIntensity;
@property (nonatomic) NSNumber* cornerEyeIntensity;
@property (nonatomic) NSNumber* mouthCornerIntensity;

-(instancetype)initWithDictionary:(NSDictionary*)dict;

@end


//YNDeformationEffect:intensity
UIKIT_EXTERN NSString* const kYNDeformationEffectPropertyFace;
//YNDeformationEffect:eyeIntensity
UIKIT_EXTERN NSString* const kYNDeformationEffectPropertyEye;
//YNDeformationEffect:noseIntensity
UIKIT_EXTERN NSString* const kYNDeformationEffectPropertyNose;
//YNEffect:chinIntensity
UIKIT_EXTERN NSString* const kYNDeformationEffectPropertyChin;
//YNDeformationEffect:foreheadIntensity
UIKIT_EXTERN NSString* const kYNDeformationEffectPropertyForehead;
//YNDeformationEffect:smallFaceIntensity
UIKIT_EXTERN NSString* const kYNDeformationEffectPropertySmallFace;
//YNDeformationEffect:cutFaceIntensity
UIKIT_EXTERN NSString* const kYNDeformationEffectPropertyCutFace;
//YNDeformationEffect:movNoseIntensity
UIKIT_EXTERN NSString* const kYNDeformationEffectPropertyMovNose;
//YNDeformationEffect:zoomMouthIntensity
UIKIT_EXTERN NSString* const kYNDeformationEffectPropertyZoomMouth;
//YNDeformationEffect:cornerEyeIntensity
UIKIT_EXTERN NSString* const kYNDeformationEffectPropertyCornerEye;
//YNDeformationEffect:mouthCornerIntensity
UIKIT_EXTERN NSString* const kYNDeformationEffectPropertyMouthCorner;
