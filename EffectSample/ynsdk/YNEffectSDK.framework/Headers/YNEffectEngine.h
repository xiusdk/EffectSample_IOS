/*----------------------------------------------------------------------------------------------
 *
 * This file is XIU's property. It contains XIU's trade secret, proprietary and
 * confidential information.
 *
 * The information and code contained in this file is only for authorized XIU employees
 * to design, create, modify, or review.
 *
 * DO NOT DISTRIBUTE, DO NOT DUPLICATE OR TRANSMIT IN ANY FORM WITHOUT PROPER AUTHORIZATION.
 *
 * If you are not an intended recipient of this file, you must not copy, distribute, modify,
 * or take any action in reliance on it.
 *
 * If you have received this file in error, please immediately notify XIU and
 * permanently delete the original and any copy of any file and any printout thereof.
 * (c) www.xiusdk.cn
 *---------------------------------------------------------------------------------------------*/

//
//  YNEffectEngine.h
//  YNEffectSDK
//
//  Created by xiu on 2018/6/20.
//  Copyright (C) 2018 ZHF. All rights reserved.
//

#import "YNEffect.h"
#import "YNTypes.h"
#import <CoreVideo/CoreVideo.h>
#import <OpenGLES/ES3/gl.h>

@interface YNEffectEngine : NSObject

@property (nonatomic, readonly) NSArray<YNEffect*>* effects;

//init engine with effects.
+(instancetype)engineWithEffects:(NSArray<YNEffect*>*)effects;
-(instancetype)initWithEffects:(NSArray<YNEffect*>*)effects;

//add an effecct.
// clear clear all old effects before add the effect.
-(void)addEffect:(YNEffect*)effect;
-(void)addEffects:(NSArray<YNEffect*>*)effects;

//add effeccts.
// clear clear all old effects before add the effects.
-(void)addEffect:(YNEffect*)effect clear:(BOOL)clear;
-(void)addEffects:(NSArray<YNEffect*>*)effects clear:(BOOL)clear;

//remove an effect.
-(BOOL)removeEffect:(YNEffect*)effect;
//remove all effects.
-(void)removeAllEffects;

// it's better to  modified effect list betweeen BeginUpdate/EndUpdate
-(void)beginUpdate;
-(void)endUpdate;

//process I420 image data.
//return a CVPixelBufferRef object if successful,
//The caller does not own the returned dataBuffer, and must retain it explicitly if the caller needs to maintain a reference to it.
-(GLuint)processVideoFrameWithY:(unsigned char*)pY y_stride:(int)y_stride U:(unsigned char*)pU u_stride:(int)u_stride
                            V:(unsigned char*)pV v_stride:(int)v_stride width:(int)width height:(int)height rotation:(YN_ROTATION_TYPE)rotation flip:(BOOL)flip faces:(YNFacesRef)faces faceCount:(int)faceCount;

//process BGRA image data.
//return a CVPixelBufferRef object if successful,
//The caller does not own the returned dataBuffer, and must retain it explicitly if the caller needs to maintain a reference to it.
-(GLuint)processVideoFrameWithBGRA:(unsigned char*)pBGRA stride:(int)stride width:(int)width height:(int)height rotation:(YN_ROTATION_TYPE)rotation flip:(BOOL)flip faces:(YNFacesRef)faces faceCount:(int)faceCount;

//process OpenGLES RGBA texture.
//return a CVPixelBufferRef object if successful,
//The caller does not own the returned dataBuffer, and must retain it explicitly if the caller needs to maintain a reference to it.
-(GLuint)processVideoFrameWithTexture:(GLuint)texture width:(int)width height:(int)height rotation:(YN_ROTATION_TYPE)rotation flip:(BOOL)flip faces:(YNFacesRef)faces faceCount:(int)faceCount;

//process OpenGLES YV12 textures.
//return a CVPixelBufferRef object if successful,
//The caller does not own the returned dataBuffer, and must retain it explicitly if the caller needs to maintain a reference to it.
-(GLuint)processVideoFrameWithYV12Textures:(GLuint*)texture width:(int)width height:(int)height rotation:(YN_ROTATION_TYPE)rotation flip:(BOOL)flip faces:(YNFacesRef)faces faceCount:(int)faceCount;

//process CVPixelBufferRef image data.
//return a CVPixelBufferRef object if successful,
//The caller does not own the returned dataBuffer, and must retain it explicitly if the caller needs to maintain a reference to it.
-(GLuint)processVideoFrameWithCVPixelBuffer:(CVPixelBufferRef)pixelBuffer rotation:(YN_ROTATION_TYPE)rotation flip:(BOOL)flip faces:(YNFacesRef)faces faceCount:(int)faceCount;

-(BOOL)renderToCVPixelBuffer:(CVPixelBufferRef)pixelBuffer faces:(YNFacesRef)faces faceCount:(int)faceCount;
@end
