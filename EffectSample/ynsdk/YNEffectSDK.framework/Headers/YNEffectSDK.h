/*----------------------------------------------------------------------------------------------
 *
 * This file is XIU's property. It contains XIU's trade secret, proprietary and
 * confidential information.
 *
 * The information and code contained in this file is only for authorized XIU employees
 * to design, create, modify, or review.
 *
 * DO NOT DISTRIBUTE, DO NOT DUPLICATE OR TRANSMIT IN ANY FORM WITHOUT PROPER AUTHORIZATION.
 *
 * If you are not an intended recipient of this file, you must not copy, distribute, modify,
 * or take any action in reliance on it.
 *
 * If you have received this file in error, please immediately notify XIU and
 * permanently delete the original and any copy of any file and any printout thereof.
 * (c) www.xiusdk.cn
 *---------------------------------------------------------------------------------------------*/

//
//  EffectSDK.h
//  EffectSDK
//
//  Created by xiu on 2018/6/20.
//  Copyright (C) 2018 ZHF. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for EffectSDK.
FOUNDATION_EXPORT double EffectSDKVersionNumber;

//! Project version string for EffectSDK.
FOUNDATION_EXPORT const unsigned char EffectSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <EffectSDK/PublicHeader.h>

#import "YNEffectType.h"
#import "YNEffect.h"
#import "YNBeautyEffect.h"
#import "YNCuteEffect.h"
#import "YNLandscapeEffect.h"
#import "YNDeformationEffect.h"
#import "YNSlenderEffect.h"
#import "YNSharpenEffect.h"
#import "YNLongLegEffect.h"
#import "YNMakeupEffect.h"
#import "YNEnhancementEffect.h"
#import "YNEffectEngine.h"



