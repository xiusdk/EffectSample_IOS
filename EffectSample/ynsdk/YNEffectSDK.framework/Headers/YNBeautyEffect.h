/*----------------------------------------------------------------------------------------------
 *
 * This file is XIU's property. It contains XIU's trade secret, proprietary and
 * confidential information.
 *
 * The information and code contained in this file is only for authorized XIU employees
 * to design, create, modify, or review.
 *
 * DO NOT DISTRIBUTE, DO NOT DUPLICATE OR TRANSMIT IN ANY FORM WITHOUT PROPER AUTHORIZATION.
 *
 * If you are not an intended recipient of this file, you must not copy, distribute, modify,
 * or take any action in reliance on it.
 *
 * If you have received this file in error, please immediately notify XIU and
 * permanently delete the original and any copy of any file and any printout thereof.
 * (c) www.xiusdk.cn
 *---------------------------------------------------------------------------------------------*/

//
//  YNBeautyEffect.h
//  YNEffectSDK
//
//  Created by xiu on 2018/6/20.
//  Copyright (C) 2018 ZHF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YNEffect.h"

@interface YNBeautyEffect : YNEffect

// 0 ~ 100.
@property (nonatomic) NSNumber* whitenIntensity;
//YNBeautyMode
@property (nonatomic) YNBeautyMode mode;

// the following parameters for YNBeautyMode_Mode2.
//0~100
@property (nonatomic) NSNumber* sharpenRatio;
//0~100
@property (nonatomic) NSNumber* lipColorRatio;
//0~100
@property (nonatomic) NSNumber* eyeBagRatio;
//0~100
@property (nonatomic) NSNumber* eyeLightRatio;

-(instancetype)initWithIntensity:(NSNumber*)intensity whitenIntensity:(NSNumber*)whitenIntensity;

@end
