============ XIUSDK 文件说明 ============
1.  本文件夹包含了要用到的库文件和滤镜的资源文件以及配置文件
2.  samples文件夹里面是一个完整的demo文件，主要实现美颜，大眼，瘦脸, 萌颜功能
3.  beauty.json landscape.json sharpen.json slender.json cute.json为子配置文件, 多个子配置文件组合可以实现叠加功能，见params.json



============ XIUSDK 功能说明 ============
1. 美颜功能（磨皮，美白）
美颜类YNBeautyEffect，类实例化
YNBeautyEffect* beautyEffect = [[YNBeautyEffect alloc] initWithConfig:[item stringByAppendingPathComponent:@"beauty.json"]];
参数见beauty.json
{
    "intensity":45,       // 磨皮强度，值[0, 100]
    "mode":"default",     // 默认，不要改动; "mode1": fast 模式
    "whitenIntensity":35  // 美白强度，值[0, 100]
}


2. 清晰度功能
YNSharpenEffect，类实例化
YNSharpenEffect* sharpen = [[YNSharpenEffect alloc] initWithConfig:[item stringByAppendingPathComponent:@"sharpen.json"]];
参数见sharpen.json
{
    "intensity":45  // 强度，值[0, 100]
}


3. 滤镜功能
滤镜类YNLandscapeEffect，类实例化
YNLandscapeEffect* landscapeEffect = [[YNLandscapeEffect alloc] initWithConfig:[item stringByAppendingPathComponent:@"landscape.json"]];
参数见landscape.json
{
    "intensity":60, // 滤镜强度，值[0, 100]
    "mode":"LUT3D", // 默认，不要改动
    "samplers":[
                "meterial/mint.png"  // 滤镜表路径，大小512x512，png格式，可以自定义
                ]
}


4. 瘦脸大眼功能
YNSlenderEffect，类实例化
YNSlenderEffect* slender = [[YNSlenderEffect alloc] initWithConfig:[item stringByAppendingPathComponent:@"slender.json"]];
参数见slender.json
{
	"intensity":60,       // 瘦脸强度，值[0, 100]
    "eyeBigIntensity":60  // 大眼强度，值[0, 100]
}

5. 长腿功能
YNLongLegEffect，类实例化
YNLongLegEffect* effect =  [[YNLongLegEffect alloc] initWithConfig:[item stringByAppendingPathComponent:@"longleg.json"]];
参数见longleg.json
{
"intensity":60,       // 强度，值[0, 100]
}

6. 萌颜功能
YNCuteEffect，类实例化
YNCuteEffect* effect =  [[YNCuteEffect alloc] initWithConfig:[item stringByAppendingPathComponent:@"cute.json"]];
参数见cute.json

============ EffectEngine 调用说明 ============
YNEffectEngine* _engine;
    
// 引擎初始化
if( _engine == nil)
{
    _engine = [YNEffectEngine new];
}

// 参数设置
__weak id engine = _engine;
dispatch_async(_captureSessionQueue, ^{
    [engine addEffects:effectArray clear:YES];
});


// 引擎调用
GLuint result = [_engine processVideoFrameWithCVPixelBuffer:pixelBuffer rotation:YN_CLOCKWISE_ROTATE_0 flip:NO faces:faces faceCount:iFaceCount];


============ facesdk 调用说明 ============
YNHandle _handle;

// 引擎初始化
if( _handle == NULL)
{
    _handle = YNFaceTrack_Initialize(0);
    NSString* resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString* track_model_path = [resourcePath stringByAppendingPathComponent:@"yn_model_track.tar"];
    YNRESULT ret = YNFaceTrack_LoadModels(_handle, [track_model_path UTF8String]);
    if (ret != YN_OK){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"错误提示" message:@"算法SDK初始化失败，可能是SDK权限过期，与绑定包名不符" delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil, nil];
        [alert show];
    }
}


// 引擎调用
iRet = YNFaceTrack_Track(_handle, pRGBA, width, height, strides, YN_PIX_FMT_RGBA8888, YN_CLOCKWISE_ROTATE_0, &faces, &iFaceCount);


// 引擎销毁
if(_handle)
{
    YNFaceTrack_Uninitialize(_handle);
    _handle = NULL;
}


============ www.xiusdk.com ============
