/*----------------------------------------------------------------------------------------------
 *
 * This file is XIU's property. It contains XIU's trade secret, proprietary and
 * confidential information.
 *
 * The information and code contained in this file is only for authorized XIU employees
 * to design, create, modify, or review.
 *
 * DO NOT DISTRIBUTE, DO NOT DUPLICATE OR TRANSMIT IN ANY FORM WITHOUT PROPER AUTHORIZATION.
 *
 * If you are not an intended recipient of this file, you must not copy, distribute, modify,
 * or take any action in reliance on it.
 *
 * If you have received this file in error, please immediately notify XIU and
 * permanently delete the original and any copy of any file and any printout thereof.
 * (c) www.xiusdk.cn
 *---------------------------------------------------------------------------------------------*/

#import <Foundation/Foundation.h>
#import "DataPickerViewController.h"

@protocol PickerDelegate <NSObject>

@property (nonatomic)NSString* title;
@property (nonatomic)UIWindow* window;

@required

-(void)presentWithAnimated:(BOOL)animated completion:(void(^)(BOOL))completion;
-(void)dismissViewControllerAnimated:(BOOL)animated;

@end

@interface DataPicker : NSObject<DataPickerViewControllerDelegate, PickerDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, readonly)DataPickerViewController* pickerViewController;

-(instancetype)initWithDataPickerViewController:(DataPickerViewController*)pickerViewController;
-(instancetype)init;

-(void)dismissViewControllerAnimated:(BOOL)animated;
-(void)presentWithAnimated:(BOOL)animated completion:(void(^)(BOOL))completion;
@end
