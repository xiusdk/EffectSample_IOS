/*----------------------------------------------------------------------------------------------
 *
 * This file is XIU's property. It contains XIU's trade secret, proprietary and
 * confidential information.
 *
 * The information and code contained in this file is only for authorized XIU employees
 * to design, create, modify, or review.
 *
 * DO NOT DISTRIBUTE, DO NOT DUPLICATE OR TRANSMIT IN ANY FORM WITHOUT PROPER AUTHORIZATION.
 *
 * If you are not an intended recipient of this file, you must not copy, distribute, modify,
 * or take any action in reliance on it.
 *
 * If you have received this file in error, please immediately notify XIU and
 * permanently delete the original and any copy of any file and any printout thereof.
 * (c) www.xiusdk.cn
 *---------------------------------------------------------------------------------------------*/

#import "CanvasView.h"
//#import "GLMakeupFace.h"
#import <CoreText/CoreText.h>

@implementation CanvasView
{
    CGContextRef context ;
}

- (void)drawRect:(CGRect)rect {
    [self drawPointWithPoints:self.arrPersons] ;
}

-(void)drawPointWithPoints:(NSArray *)arrPersons
{
    if (context) {
        CGContextClearRect(context, self.bounds) ;
    }
    context = UIGraphicsGetCurrentContext();
    
    char strNum[10];
    
    CGFloat s = 18;
    CTFontRef ctfont = CTFontCreateWithName(CFSTR("PostScript"), s, NULL);
    CGColorRef ctColor = [[UIColor greenColor] CGColor];
    
    // Create an attributed string
    CFStringRef keys[] = { kCTFontAttributeName,kCTForegroundColorAttributeName };
    CFTypeRef values[] = { ctfont,ctColor};
    CFDictionaryRef attr = CFDictionaryCreate(NULL, (const void **)&keys, (const void **)&values,
                                              sizeof(keys) / sizeof(keys[0]), &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
    
    for( NSDictionary* face in arrPersons)
    {
        int count = 0;
        NSArray* points = [face objectForKey:POINTS_KEY];
        for( NSValue* pt in points)
        {
            sprintf(strNum, "%d", count);
            
            CGPoint p = [pt CGPointValue];
            p = CGPointMake(p.x*self.factor, p.y*self.factor);
            
            CGContextAddEllipseInRect(context, CGRectMake(p.x - 1 , p.y - 1 , 2 , 2));
            CFStringRef ctStr = CFStringCreateWithCString(nil, strNum, kCFStringEncodingUTF8);
            
            CFAttributedStringRef attrString = CFAttributedStringCreate(NULL,ctStr, attr);
            CTLineRef line = CTLineCreateWithAttributedString(attrString);
            CGContextSetTextMatrix(context, CGAffineTransformMakeScale(1.0, -1.0)); //Use this one if the view's coordinates are flipped
            CGContextSetTextPosition(context, p.x, p.y);
            CTLineDraw(line, context);
            
            CFRelease(line);
            CFRelease(attrString);
            CFRelease(ctStr);
            
            count++;

        }
        CGRect outline = [[face objectForKey:RECT_KEY] CGRectValue];
        CGRect rect = CGRectMake(outline.origin.x*self.factor, outline.origin.y*self.factor, outline.size.width*self.factor, outline.size.height*self.factor);
        CGContextAddRect(context, rect) ;
    }
    
    CFRelease(ctfont);
    CFRelease(attr);
    
    [[UIColor greenColor] set];
    CGContextSetLineWidth(context, 2);
    CGContextStrokePath(context);
}

@end

