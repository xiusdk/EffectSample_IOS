/*----------------------------------------------------------------------------------------------
 *
 * This file is XIU's property. It contains XIU's trade secret, proprietary and
 * confidential information.
 *
 * The information and code contained in this file is only for authorized XIU employees
 * to design, create, modify, or review.
 *
 * DO NOT DISTRIBUTE, DO NOT DUPLICATE OR TRANSMIT IN ANY FORM WITHOUT PROPER AUTHORIZATION.
 *
 * If you are not an intended recipient of this file, you must not copy, distribute, modify,
 * or take any action in reliance on it.
 *
 * If you have received this file in error, please immediately notify XIU and
 * permanently delete the original and any copy of any file and any printout thereof.
 * (c) www.xiusdk.cn
 *---------------------------------------------------------------------------------------------*/

#import "DataPickerViewController.h"

@interface DataPickerViewController()
{
    NSString* _title;
}
@end
@implementation DataPickerViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self)
    {
        
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.titleLabel.text = _title;
    self.view.backgroundColor = [UIColor clearColor];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

-(void)setTitle:(NSString *)title
{    
    [super setTitle:title];
    _title = title;
    self.titleLabel.text = title;
}

- (IBAction)cancelClicked:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES];
}

- (IBAction)okClicked:(UIButton *)sender {
    
    if( _delegate && [_delegate respondsToSelector:@selector(shouldDismissViewController:)])
    {
        if(![_delegate shouldDismissViewController:self])
            return;
    }
    [self dismissViewControllerAnimated:YES];
}

-(void)dismissViewControllerAnimated:(BOOL)animated;
{
    if( _delegate && [_delegate respondsToSelector:@selector(dismissViewController:animated:)])
    {
        [_delegate dismissViewController:self animated:animated];
    }
}
@end
