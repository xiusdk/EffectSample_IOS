/*----------------------------------------------------------------------------------------------
 *
 * This file is XIU's property. It contains XIU's trade secret, proprietary and
 * confidential information.
 *
 * The information and code contained in this file is only for authorized XIU employees
 * to design, create, modify, or review.
 *
 * DO NOT DISTRIBUTE, DO NOT DUPLICATE OR TRANSMIT IN ANY FORM WITHOUT PROPER AUTHORIZATION.
 *
 * If you are not an intended recipient of this file, you must not copy, distribute, modify,
 * or take any action in reliance on it.
 *
 * If you have received this file in error, please immediately notify XIU and
 * permanently delete the original and any copy of any file and any printout thereof.
 * (c) www.xiusdk.cn
 *---------------------------------------------------------------------------------------------*/

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <YNEffectSDK/YNEffectSDK.h>

@interface SettingsViewController : UITableViewController

@property (nonatomic, weak)YNBeautyEffect* beautyParam;
@property (nonatomic, weak)YNLandscapeEffect* landscapeParam;
    @property (nonatomic, weak)YNSlenderEffect* slenderParam;
@property (nonatomic, weak)YNSharpenEffect* sharpenParam;

@property (nonatomic)AVCaptureSession* captureSession;
@property (nonatomic)BOOL  runBenchmark;

-(void)reloadData;
    
@end
