//
//  main.m
//  YNEffectSample
//
//  Created by xiu on 2017/9/25.
//  Copyright © 2017年 xiu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
