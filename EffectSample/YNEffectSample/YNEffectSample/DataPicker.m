/*----------------------------------------------------------------------------------------------
 *
 * This file is XIU's property. It contains XIU's trade secret, proprietary and
 * confidential information.
 *
 * The information and code contained in this file is only for authorized XIU employees
 * to design, create, modify, or review.
 *
 * DO NOT DISTRIBUTE, DO NOT DUPLICATE OR TRANSMIT IN ANY FORM WITHOUT PROPER AUTHORIZATION.
 *
 * If you are not an intended recipient of this file, you must not copy, distribute, modify,
 * or take any action in reliance on it.
 *
 * If you have received this file in error, please immediately notify XIU and
 * permanently delete the original and any copy of any file and any printout thereof.
 * (c) www.xiusdk.cn
 *---------------------------------------------------------------------------------------------*/

#import "DataPicker.h"

#define SCREEN_HEIGHT MAX([[UIScreen mainScreen] bounds].size.width,[[UIScreen mainScreen] bounds].size.height)
#define SCREEN_WIDTH MIN([[UIScreen mainScreen] bounds].size.width,[[UIScreen mainScreen] bounds].size.height)
#define SCREEN_SCALE  [UIScreen mainScreen].scale
#define SCALE_X(x)  ((x)*SCREEN_WIDTH/360)
#define SCALE_CEIL_X(x)  (ceil((x)*SCREEN_WIDTH/360))

@interface DataPicker()
{
    UIWindow* _window;
    DataPickerViewController* _pickerViewController;
}
@end

@implementation DataPicker

@synthesize window = _window;
@synthesize title;
@synthesize pickerViewController = _pickerViewController;

-(instancetype)initWithDataPickerViewController:(DataPickerViewController*)pickerViewController
{
    self = [super init];
    if(self)
    {
        pickerViewController.delegate = self;
        _pickerViewController = pickerViewController;
        pickerViewController.title = self.title;
        _window = [[UIWindow alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _window.windowLevel = UIWindowLevelNormal;
        _window.alpha = 0;
        _window.hidden = NO;
        _window.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        pickerViewController.view.transform = CGAffineTransformMakeTranslation(0, 180+SCALE_X(40));
        self.pickerViewController.ContainerHeightLayoutConstraint.constant = SCALE_X(40) + 180;
        UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:pickerViewController];
        nc.view.backgroundColor = [UIColor clearColor];
        [pickerViewController.navigationController setNavigationBarHidden:YES animated:NO];
        _window.rootViewController = nc;
    }
    return self;
}

-(instancetype)init
{
    return [self initWithDataPickerViewController:[[DataPickerViewController alloc] initWithNibName:@"DataPickerViewController" bundle:[NSBundle mainBundle]]];
}

-(void)dealloc
{
    self.pickerViewController.pickerView.dataSource = nil;
    self.pickerViewController.pickerView.delegate = nil;
}

-(void)setTitle:(NSString *)newTitle;
{
    title = newTitle;
    _pickerViewController.title = newTitle;
}

-(void)dismissViewController:(DataPickerViewController*)pickerViewController animated:(BOOL)animated
{
    [self dismissViewControllerAnimated:animated];
}

-(void)dismissViewControllerAnimated:(BOOL)animated
{
    if( _window == nil)
        return;
    
    DataPickerViewController* vc = _pickerViewController;
    UIWindow* window = _window;

    _pickerViewController = nil;
    _window = nil;
    
    if( animated)
    {
        [UIView animateWithDuration:0.3
                         animations:^{
                             vc.view.transform  = CGAffineTransformMakeTranslation(0, vc.view.frame.size.height);
                             window.alpha  = 0;
                         } completion:^(BOOL finished){
                             window.hidden = YES;
                             [vc.view removeFromSuperview];
                             window.rootViewController = nil;
                         }];
    }
    else
    {
        window.alpha = 0;
        window.hidden = YES;
        [vc.view removeFromSuperview];
        window.rootViewController = nil;
    }
}

-(void)presentWithAnimated:(BOOL)animated completion:(void(^)(BOOL))completion
{
    self.pickerViewController.pickerView.dataSource = self;
    self.pickerViewController.pickerView.delegate = self;
    
    if( animated )
    {
        [UIView animateWithDuration:0.3
                         animations:^{
                             self->_pickerViewController.view.transform  = CGAffineTransformIdentity;
                             self->_window.alpha  = 1;
                         } completion:completion];
    }
    else
    {
        _pickerViewController.view.transform  = CGAffineTransformIdentity;
        _window.alpha  = 1;
        if( completion )
        {
            completion(YES);
        }
    }
}

#pragma make- UIPickerViewDataSource
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 0;
}

#pragma make- UIPickerViewDelegate
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return SCALE_CEIL_X(40);
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [pickerView reloadComponent:component];
}

@end
